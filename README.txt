Payl8r calculator usage

Requires jQuery. Include both pl-calculator.js and plcalculator.css or minified versions.




Provides function plcalc.make_calculator. Draws full screen calculator with link.

Parameter 1: Description. (Array)

Parameter 2: Price. (Integer or Float)

Parameter 3: Link. This is the link that is followed after clicking next. (String)

Parameter 4: Link text. Normally 'Continue'. (String)

Example: plcalc.make_calculator(['1 x Leather Jacket (Colour: Black)', '2 x Wig', 'Standard shipping'], 150, 'https://google.co.uk', 'Continue');




Provides function plcalc.draw_to_frame. Draws calculator to an HTML element, normally used on product page.

Parameter 1: item_cost (Integer or Float)

Parameter 2: quantity_input. This should be set to the jQuery object e.g. $('#quantity') for the quantity input on the page. (Object)

Parameter 3: Element to draw too, jQuery object e.g. $('#element'). (Object)




See demo.html for an example.