# Payl8r Loan Calculator #

### What is this repository for? ###

This is a loan calculator that automatically pulls parameters from an API published on payl8r.com and generates a simple loan calculator for customers to use on an e-commerce website.

### How do I get set up? ###

See README.txt. Examples of usage are included in demo1.html, demo2.html and demo3.html.

### Contribution ###

Created by Luke Roughley for Social Money Ltd

The calculator includes and requires jquery (https://jquery.com/).

The loan calculator is open for public development.